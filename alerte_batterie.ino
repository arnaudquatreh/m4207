#include <LBattery.h>
#define LED 2 //connect LED to digital pin2
char buff[256];

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  // initialize the digital pin2 as an output.
  pinMode(LED, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  sprintf(buff,"battery level = %d", LBattery.level() );
  Serial.println(buff);
  sprintf(buff,"is charging = %d",LBattery.isCharging() );
  Serial.println(buff);
  delay(1000); 
  if(LBattery.level()<=15) {
    sprintf(buff,"Batterie faible ! (%d%%)",LBattery.level() );
    Serial.println(buff);
    digitalWrite(LED, HIGH);   // set the LED on
    delay(1000);               // for 1000ms
    digitalWrite(LED, LOW);   // set the LED off
  }
}